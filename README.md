# DSA2000 KiCAD Library

To install, clone this repository (or download a zip/tarall) and place it somewhere reasonable on your computer. Then, following the [KiCAD documentation](https://docs.kicad.org/6.0/en/pcbnew/pcbnew_footprints_and_libraries.html), add both the symbol library (located in `symbols/DSA2K.kicad_sym`) and the footprint library (located in `footprints/DSA2k.pretty/`) to the global symbol and footprint libraries.

!!! TODO
Figure out how to point to the 3D models correctly